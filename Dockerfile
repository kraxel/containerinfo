FROM registry.access.redhat.com/ubi8/ubi

ENV SUMMARY="containerinfo" \
    DESCRIPTION="some info pages @ port 8080"

LABEL maintainer="Gerd Hoffmann <kraxel@redhat.com>" \
      summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.display-name="${SUMMARY}" \
      io.k8s.description="${DESCRIPTION}" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="httpd"

USER root

RUN dnf update -y && \
    dnf install -y httpd pciutils procps-ng \
                   iproute iputils bind-utils \
		   /usr/bin/bootctl \
		   /usr/sbin/lshw \
    && \
    dnf clean all -y

RUN mkdir -p /run/httpd;\
    chmod 777 /run/httpd /etc/httpd/logs /var/www/html;\
    sed -i -e '/Listen/s/80/8080/' /etc/httpd/conf/httpd.conf

COPY bin/* /usr/local/bin/

USER 1001
EXPOSE 8080

CMD ["/usr/local/bin/run"]
