DESTDIR	?=
prefix	:= /usr/local
bindir	:= $(prefix)/bin
sysdir	:= $(prefix)/lib/systemd/system
docroot	:= /var/www/html
htmldir	:= $(docroot)/containerinfo

gen	:= systemd/containerinfo.service
service	:= systemd/containerinfo.service
service	+= systemd/containerinfo.timer

all build: $(gen)

clean:
	rm -rf $(gen)
	rm -rf test
	rm -f *~ bin/*~ systemd/*~

systemd/%.service: systemd/%.service.in
	sed	-e "s|@bindir@|$(bindir)|" \
		-e "s|@htmldir@|$(htmldir)|" \
		< $< > $@

install: $(service)
	install -d $(DESTDIR)$(bindir)
	install -d $(DESTDIR)$(htmldir)
	install -d $(DESTDIR)$(sysdir)
	install -m 755 bin/containerinfo $(DESTDIR)$(bindir)
	install -m 644 $(service) $(DESTDIR)$(sysdir)

enable: install
	systemctl daemon-reload
	systemctl enable --now containerinfo.timer

.PHONY: test
test:
	mkdir -p test
	bin/containerinfo test

