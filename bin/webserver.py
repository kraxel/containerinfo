#!/usr/bin/python
import os
import socket
import optparse

from http.server import SimpleHTTPRequestHandler
try:
    from http.server import ThreadingHTTPServer as HTTPServer
    protocol = "HTTP/1.1"
except ImportError:
    from http.server import HTTPServer
    protocol = "HTTP/1.0"

parser = optparse.OptionParser()
parser.add_option('-d', '--directory', dest = 'directory', type = 'string', default = '.',
                  help = 'server files from DIR', metavar = 'DIR')
parser.add_option('-p', '--port', dest = 'port', type = 'int', default = 8080,
                  help = 'bind to tcp port PORT', metavar = 'PORT')
parser.add_option('-b', '--bind', dest = 'bind', type = 'string', default ='',
                  help = 'bind to tcp addr ADDR', metavar = 'ADDR')
(options, args) = parser.parse_args()

print(f'config: dir="{options.directory}", addr="{options.bind}", port={options.port}')

handler = SimpleHTTPRequestHandler
handler.protocol_version = protocol
server = HTTPServer((options.bind, options.port), handler)

os.chdir(options.directory)
server.serve_forever()
